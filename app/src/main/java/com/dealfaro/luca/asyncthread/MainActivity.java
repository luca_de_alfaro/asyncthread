package com.dealfaro.luca.asyncthread;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    MyComputeTask myAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        if (myAsyncTask != null) {
            myAsyncTask.cancel(true);
        }
        super.onPause();
    }

    public void enterNumber(View v) {
        EditText et = (EditText) findViewById(R.id.editText);
        int i = 0;
        boolean ok = false;
        try {
            i = Integer.parseInt(et.getText().toString());
            ok = true;
        } catch (Throwable t) {
        }
        ;
        // Clears the text.
        et.setText("");
        if (ok && i > 0) {
            if (myAsyncTask != null) {
                myAsyncTask.cancel(true);
            }
            myAsyncTask = new MyComputeTask();
            myAsyncTask.execute(new Integer(i));
        }

    }

    class MyComputeTask extends AsyncTask<Integer, Integer, Boolean> {
        protected Boolean doInBackground(Integer... integers) {
            // This runs in a background thread.
            // Fish out the integer.
            int k = integers[0];
            // Does the computation
            while (k > 1 && !isCancelled()) {
                if (k % 2 == 0) {
                    k = k / 2;
                } else {
                    k = k * 3 + 1;
                }
                try {
                    Thread.sleep(1000);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                if (!isCancelled()) {
                    publishProgress(Integer.valueOf(k));
                }
            }
            return new Boolean(true); // Not really useful.
        }

        protected void onProgressUpdate(Integer... i) {
            // Here we are in the main UI thread.
            TextView tv = (TextView) findViewById(R.id.textView);
            tv.setText("" + i[0]);
        }

        protected void onPostExecute(Boolean b) {
            // UI thread.
            // We can do a toast.
            Toast toast = Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

}